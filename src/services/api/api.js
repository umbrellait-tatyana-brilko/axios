import * as axios from "axios";

const instance = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com",
  timeout: 1000,
  // headers: {'X-Custom-Header': 'foobar'}
});

export function getAllUsers() {
  return instance.get("/users/").then((response) => response.data);
}

export function getUserDetails(id) {
  return instance.get(`/users/${id}`).then((response) => response.data);
}
