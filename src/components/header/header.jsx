import React from "react";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <div>
      <NavLink to="/axios">AXIOS |</NavLink>
      <NavLink to="/posts"> | CONTEXT | </NavLink>
      <NavLink to="/photos">| ROUTER</NavLink>
    </div>
  );
};

export default Header;
