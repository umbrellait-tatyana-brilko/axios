import React, { useState, useEffect } from "react";

import { getAllUsers } from "../../../services/api/api";

import "./userDataList.css";

function UsersDataList(props) {
  const [usersList, setUsersList] = useState(null);

  useEffect(() => {
    getAllUsers()
      .then((data) => {
        const allUsers = data;
        setUsersList(allUsers);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [setUsersList]);

  if (!usersList || usersList.length === 0)
    return <p> Список пользователей не получен</p>;

  const usersData = usersList.map(({ id, name, username }) => {
    return (
      <li
        key={id}
        onClick={() => props.selectedPerson(id)}
        className={"fieldList"}
      >
        {id}. {username} <span className={"pale"}>&#040;{name} &#041;</span>
      </li>
    );
  });

  return (
    <div>
      <ul className="list">{usersData}</ul>
    </div>
  );
}

export default UsersDataList;
