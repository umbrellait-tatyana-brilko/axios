import React, { useState, useEffect } from "react";

import "./userDetails.css";
import { getUserDetails } from "../../../services/api/api";

const UserDetails = (props) => {
  const selectedUser = props;
  const [userDetails, setUserDetails] = useState(0);

  let id = Number(Object.values(selectedUser));

  useEffect(() => {
    let isComponentMounted = true;

    getUserDetails(id).then((data) => {
      const detailsUser = data;
      if (isComponentMounted) {
        setUserDetails(detailsUser);
      }
    });

    return () => {
      isComponentMounted = false;
    };
  }, [id]);

  if (!userDetails) {
    return <p> No data about user</p>;
  }

  return (
    <ul className={"cardUser"}>
      <li>{userDetails.username}</li>
      <li>
        <span className={"namingLine"}>Name:</span> {userDetails.name}
      </li>
      <li>
        <span className={"namingLine"}>E-mail:</span> {userDetails.email}
      </li>
      <li>
        <span className={"namingLine"}>Phone:</span> {userDetails.phone}
      </li>
      <li>
        <span className={"namingLine"}>Web-site:</span> {userDetails.website}
      </li>
    </ul>
  );
};

export default UserDetails;
