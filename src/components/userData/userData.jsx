import React, { useState } from "react";

import UsersDataList from "./userDataList/userDataList";
import UserDetails from "./userDetails/userDetails";
import Modal from "../modals/modal";

const UsersData = () => {
  const [modalClose, setModalClose] = useState(false);
  const [selectedUser, setSelectedUser] = useState(0);

  const closingModal = () => {
    setModalClose(false);
  };

  // Select a user from the list
  const modalUserDataHandler = (id) => {
    setModalClose(true);
    setSelectedUser(id);
  };

  console.log(selectedUser);

  return (
    <div>
      <Modal
        title={"User card"}
        isOpened={modalClose}
        onModalClose={closingModal}
      >
        <UserDetails userId={selectedUser} />
      </Modal>
      <h2> Axios: Geting data with server</h2>
      <UsersDataList selectedPerson={modalUserDataHandler}></UsersDataList>
    </div>
  );
};

export default UsersData;
