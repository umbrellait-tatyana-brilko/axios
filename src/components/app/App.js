import React from "react";
import { Route, BrowserRouter } from "react-router-dom";

import UsersData from "../userData/userData";
import Posts from "../posts/posts";
import Photos from "../photos/photos";
import Header from "../header/header";

import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Route path="/axios" component={UsersData} />
        <Route path="/posts" component={Posts} />
        <Route path="/photos" component={Photos} />
      </div>
    </BrowserRouter>
  );
}

export default App;
