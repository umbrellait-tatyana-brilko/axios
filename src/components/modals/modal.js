import React from "react";

import "./modal.css";

const Modal = (props) => {
  return (
    <div
      className={`modal__wrapper ${props.isOpened ? "open" : "close"}`}
      style={props.style}
    >
      <div className={"modal__body"}>
        <div className={"modal__close"} onClick={props.onModalClose}>
          <i className="fas fa-times-circle"></i>
        </div>

        <h3>{props.title}</h3>
        {props.children}
      </div>
    </div>
  );
};

export default Modal;
